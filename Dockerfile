# https://hub.docker.com/_/gradle
FROM gradle:jdk14 as builder

# Copy local code to the container image.
COPY build.gradle.kts .
COPY settings.gradle.kts .
COPY src ./src

# Build a release artifact.
RUN gradle clean build --no-daemon

FROM adoptopenjdk/openjdk14:alpine-slim

# Copy the jar to the production image from the builder stage.
COPY --from=builder /home/gradle/build/libs/*-all.jar /polite-bot.jar

# Run the web service on container startup.
CMD [ "java", "-jar", "-Djava.security.egd=file:/dev/./urandom", "/polite-bot.jar" ]