import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.0"
    application
    id("com.github.johnrengelman.shadow") version "6.0.0"
}
group = "me.al"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}
dependencies {
    implementation("edu.stanford.nlp:stanford-corenlp:4.0.0")
    implementation("edu.stanford.nlp:stanford-corenlp:4.0.0:models")
    implementation("net.dean.jraw:JRAW:1.1.0")
    implementation("dev.nohus:AutoKonfig:1.0.0")
    testImplementation(kotlin("test-junit"))
}
tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "14"
}
application {
    mainClassName = "MainKt"
}