import edu.stanford.nlp.process.PTBTokenizer
import edu.stanford.nlp.simple.Document
import kotlin.random.Random


class Politizer {

    private val protected = setOf(
            "be", "was", "were", "been",
            "have", "had",
            "do", "did", "done",
            "will", "would",
            "get", "got",
            "go", "went", // ,"gone"?
    )

    fun politize(message: String): String {
        val doc = Document(message)
        return doc.sentences().joinToString(" ") {
            it.words()
                    .zip(it.nerTags())
                    .zip(it.posTags()) { pair, pos -> Triple(pair.first, pair.second, pos) }
                    .map { (word, ner, pos) -> processWord(word, pos, ner) }
                    .let { PTBTokenizer.ptb2Text(it) }
        }
    }

    private fun processWord(word: String, pos: String, ner: String) =
            if (ner == "O" && !protected.contains(word.toLowerCase()) && wordIsImpolite())
                matchPos(word, pos)
            else
                word

    private fun matchPos(word: String, pos: String) = when (pos) {
        "NN", "VB" -> "${word[0]}-word"
        "NNS", "VBS" -> "${word[0]}-words"
        "VBG" -> "${word[0]}-wording"
        "VBD", "VBN" -> "${word[0]}-worded"
        "RB" -> if (word.endsWith("ing")) "${word[0]}-wording" else "${word[0]}-word"
        else -> word
    }

    private fun wordIsImpolite() = Random.nextDouble() > 0.3 // Totally accurate
}