import dev.nohus.autokonfig.types.LongSetting
import dev.nohus.autokonfig.types.StringSetting
import net.dean.jraw.http.OkHttpNetworkAdapter
import net.dean.jraw.http.UserAgent
import net.dean.jraw.models.Comment
import net.dean.jraw.models.Submission
import net.dean.jraw.oauth.Credentials
import net.dean.jraw.oauth.OAuthHelper.automatic
import java.util.*
import kotlin.concurrent.schedule

const val tail = """

---
^I ^am ^a ^bot. ^Call ^me ^with ^u/​p-word-bot"""

fun main(args: Array<String>) {
    val userAgent = UserAgent("JRAW", "me.o.o.p-word-bot", "v1.0", "/u/on_off_")
    val username by StringSetting()
    val password by StringSetting()
    val clientId by StringSetting()
    val clientSecret by StringSetting()
    val pollPeriod by LongSetting(30)
    val credentials = Credentials.script(
        username, password, clientId, clientSecret
    )
    val adapter = OkHttpNetworkAdapter(userAgent)
    val reddit = automatic(adapter, credentials)
    val p = Politizer()
    val timer = Timer()
    timer.schedule(0, pollPeriod * 1000) {
        try {
            val page = reddit
                .me()
                .inbox()
                .iterate("mentions")
                .limit(100)
                .build().first() // can't have s-word with r-word api, serve only last 100
            val unread = page.filter { it.isUnread }
            if (unread.isNotEmpty()) {
                reddit.me().inbox().markRead(true, unread.first().uniqueId, *unread.map { it.uniqueId }.toTypedArray())
            }
            for (message in unread.filter { it.author != username }) {
                val comment = reddit.lookup(message.fullName).firstOrNull() as? Comment ?: continue
                val targetText = when (val parent = reddit.lookup(comment.parentFullName).firstOrNull()) {
                    is Comment -> parent.body
                    is Submission -> parent.selfText
                    else -> continue
                }
                targetText
                    ?.let { p.politize(it) }
                    ?.let { comment.toReference(reddit).reply(it + tail) }
            }
        } catch (ex: Exception) {
            println(ex)
        }
    }

}